# test.py
def test_alpine_os(host):
    assert host.file("/etc/os-release").contains("Alpine Linux")

def test_nginx_installed(host):
    nginx_package = host.package("nginx")
    assert nginx_package.is_installed
    assert nginx_package.version.startswith('1.16.1')   

